package utilities;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Hashtable;

/**
 * Description:
 * Class used for the driver creation. Can create both a local driver and a remote driver using the Browser Stack credentials.
 */
public class DriverFactory {

    private static boolean isWindows = System.getProperty("os.name").toLowerCase().contains("win");

    public static final String USERNAME = "christophersols1";
    public static final String AUTOMATE_KEY = "QNy6dvNfGPZnkYRGhcVm";
    public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

    /**
     * Description:
     * Method for the driver creation. Receives as input parameters all the configuration
     * needed to either set a local driver or a remote driver.
     * @param browser: desired browser
     * @param headless: boolean flag if the headless mode is going to be used or not
     * @param remoteTesting: boolean flag if the remote driver going to to be used or not
     * @param bSCapabilities: HashTable object containing the capabilities needed for Browser Stack
     * @return WebDriver object with the driver
     * @throws MalformedURLException
     */
    public static WebDriver open(String browser, boolean headless, boolean remoteTesting, Hashtable<String, String> bSCapabilities) throws MalformedURLException {

        WebDriver driver;

        try {

            if(remoteTesting){

                DesiredCapabilities capabilities = new DesiredCapabilities();

                if (browser.equalsIgnoreCase("chrome")) {

                    // Set a remote Chrome browser
                    capabilities.setCapability("browserName", "Chrome");
                    capabilities.setCapability("browserVersion", "83.0");

                } else if (browser.equalsIgnoreCase("firefox")) {

                    // Set a remote Firefox browser
                    capabilities.setCapability("browserName", "Firefox");
                    capabilities.setCapability("browserVersion", "77.0");

                } else if (browser.equalsIgnoreCase("safari")) {

                    // Set a remote Safari browser
                    if (bSCapabilities.get("os").equalsIgnoreCase("OS X")){

                        if(bSCapabilities.get("osVersion").equalsIgnoreCase("Mojave")){

                            capabilities.setCapability("browserName", "Safari");
                            capabilities.setCapability("browserVersion", "12.1");

                        } else if(bSCapabilities.get("osVersion").equalsIgnoreCase("Catalina")){

                            capabilities.setCapability("browserName", "Safari");
                            capabilities.setCapability("browserVersion", "13.1");

                        }else{

                            return null;
                        }

                    } else {

                        return null;
                    }

                } else if (browser.equalsIgnoreCase("ie")) {

                    // Set a remote IE browser
                    if (bSCapabilities.get("os").equalsIgnoreCase("Windows") &&
                            bSCapabilities.get("osVersion").equalsIgnoreCase("10")) {

                        capabilities.setCapability("browserName", "IE");
                        capabilities.setCapability("browserVersion", "11.0");

                    } else {

                        return null;
                    }

                } else if(browser.equalsIgnoreCase("edge")) {

                    // Set a remote Edge browser
                    if (bSCapabilities.get("os").equalsIgnoreCase("Windows") &&
                            bSCapabilities.get("osVersion").equalsIgnoreCase("10")) {

                        capabilities.setCapability("browserName", "Edge");
                        capabilities.setCapability("browserVersion", "83.0");

                    } else {

                        return null;
                    }

                } else {

                    return null;
                }

                // Set Browser Stack capabilities
                HashMap<String, Object> browserStackOptions = new HashMap<String, Object>();
                browserStackOptions.put("os", bSCapabilities.get("os"));
                browserStackOptions.put("osVersion", bSCapabilities.get("osVersion"));
                browserStackOptions.put("projectName", bSCapabilities.get("projectName"));
                browserStackOptions.put("buildName", bSCapabilities.get("buildName"));
                browserStackOptions.put("sessionName", bSCapabilities.get("sessionName"));
                browserStackOptions.put("local", "false");
                browserStackOptions.put("timezone", "Costa_Rica");
                browserStackOptions.put("seleniumVersion", "3.141.59");
                capabilities.setCapability("bstack:options", browserStackOptions);
                capabilities.setCapability("resolution", "1920x1080");

                // Start a remote browser
                driver = new RemoteWebDriver(new URL(URL), capabilities);

            }else {

                if (browser.equalsIgnoreCase("chrome")) {

                    // Start a local Chrome browser
                    WebDriverManager.chromedriver().setup();

                    ChromeOptions options = new ChromeOptions();
                    if (headless) {

                        options.addArguments("--headless");
                    }

                    options.addArguments("--disable-popup-blocking", "--start-maximized", "--kiosk", "--start-fullscreen");
                    DesiredCapabilities capabilities = new DesiredCapabilities();
                    capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                    driver = new ChromeDriver(options);

                } else if (browser.equalsIgnoreCase("firefox")) {

                    // Start a local Firefox browser
                    WebDriverManager.firefoxdriver().setup();

                    FirefoxOptions firefoxOptions = new FirefoxOptions();
                    FirefoxProfile profile = new FirefoxProfile();
                    profile.setPreference("browser.download.folderList", 2);
                    profile.setPreference("browser.download.dir", System.getProperty("user.home")+ "/Downloads/");
                    profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream;application/zip;text/csv;application/excel");

                    firefoxOptions.setProfile(profile);

                    if (headless) {

                        FirefoxBinary firefoxBinary = new FirefoxBinary();
                        firefoxBinary.addCommandLineOptions("--headless", "--kiosk");
                        firefoxOptions.setBinary(firefoxBinary);
                    }

                    driver = new FirefoxDriver(firefoxOptions);

                } else if (browser.equalsIgnoreCase("safari")) {

                    // Start a local Safari browser
                    driver = new SafariDriver();

                } else if (browser.equalsIgnoreCase("ie")) {

                    // Start a local IE browser
                    if (isWindows) {

                        WebDriverManager.iedriver().setup();
                        InternetExplorerOptions options = new InternetExplorerOptions();
                        options.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
                        driver = new InternetExplorerDriver(options);

                    } else {

                        return null;
                    }

                } else {

                    driver = null;
                }
            }

        }catch(WebDriverException | MalformedURLException e){

            throw e;
        }

        return driver;
    }
}