package io.mylift.pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ClassWorkspacePage {

    private WebDriver driver;

    //region Elements
    @FindBy(xpath = "//button[contains(., 'Create Project')]")
    private WebElement createProjectButton;

    @FindBy(linkText = "Individual")
    private WebElement individualButton;

    @FindBy(id = "project_template_name")
    private WebElement projectNameField;

    @FindBy(className = "trumbowyg-editor")
    private WebElement descriptionTextBox;

    @FindBy(className = "select2-search__field")
    private WebElement tagsField;

    @FindBy(css = ".save-btn")
    private WebElement saveButton;

    @FindBy(className = "message")
    private WebElement alertMessage;
    //endregion

    //region Getters
    public WebElement getAlertMessage() {
        return alertMessage;
    }
    //endregion

    //region Methods
    public void createNewProject(String projectName, String projectDescription) {

        createProjectButton.click();
        individualButton.click();

        projectNameField.sendKeys(projectName);

        // Fills the description textbox
        ((JavascriptExecutor)driver).executeScript("arguments[0].innerHTML = arguments[1]", descriptionTextBox, projectDescription);

        tagsField.sendKeys("demo", Keys.ENTER);
        saveButton.submit();
    }

    public boolean projectCreatedSuccessfully() {

        boolean result = false;

        try {

            result = alertMessage.getText().contains("The project has been created");

        } catch (NoSuchElementException e) { }

        return result;
    }
    //endregion

    public ClassWorkspacePage(WebDriver driver) {

        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
