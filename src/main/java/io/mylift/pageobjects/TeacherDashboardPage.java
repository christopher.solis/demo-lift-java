package io.mylift.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TeacherDashboardPage {

    private WebDriver driver;

    //region Elements
    @FindBy(xpath = "//button[contains(., 'Select Class')]")
    private WebElement selectClassDropDown;
    //endregion

    //region Methods
    public void selectClassByName(String className) {

        String xpath = String.format("//a[text()='%s']", className);

        selectClassDropDown.click();
        driver.findElement(By.xpath(xpath)).click();
    }

    public boolean isTeacherDashboard() {

        String currentURL = driver.getCurrentUrl();

        return currentURL.contains("teacher") && currentURL.contains("dashboard");
    }
    //endregion

    public TeacherDashboardPage(WebDriver driver) {

        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
