package io.mylift.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import javax.xml.ws.WebEndpoint;

public class StudentPortfolio {

    private WebDriver driver;

    //region Elements

    @FindBy(xpath = "//div[@class=\"level-card\"]//div[@class=\"content\"]//h4[contains(text(),'Getting Started')]")
    private WebElement gettingStartedLevel;

    @FindBy(xpath = "//div[@class=\"level-card\"]//div[@class=\"content\"]//h4[contains(text(),'Progressing')]")
    private WebElement progressingdLevel;

    @FindBy(xpath = "//div[@class=\"level-card\"]//div[@class=\"content\"]//h4[contains(text(),'Proficient')]")
    private WebElement proficientLevel;

    @FindBy(xpath = "//div[@class=\"level-card\"]//div[@class=\"content\"]//h4[contains(text(),'Distinguished')]")
    private WebElement distinguishedLevel;

    @FindBy(css = ".btn.rate-btn")
    private WebElement rateButton;

    @FindBy(css = ".btn.yes")
    private WebElement yesButton;

    @FindBy(xpath = "/html/body/div/div[3]/div/div[3]/div/div[2]")
    private WebElement notification;

    //endregion

    //region Methods

    public void gettingStartedLevelClick(){
        gettingStartedLevel.click();
    }

    public void rateButtonClick(){
        rateButton.click();
    }

    public void yesButtonClick(){
        yesButton.click();
    }

    public String getNotification(){
        return notification.getText();
    }
    //endregion

    public StudentPortfolio(org.openqa.selenium.WebDriver driver) {

        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
