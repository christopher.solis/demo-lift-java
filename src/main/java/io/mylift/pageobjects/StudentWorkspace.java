package io.mylift.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StudentWorkspace {

    private WebDriver driver;

    //region Elements
    @FindBy(css = ".btn.add-btn.expand-btn")
    private WebElement expandAllButton;

    @FindBy(xpath = "//*[@id=\"91ec69d9-b1fa-4da1-a03c-ec5f4d0a7434\"]/div[2]/a")
    private WebElement skillLink;

    //endregion

    //region Methods
    public void expandAllButtonClick() {
        expandAllButton.click();
    }

    public void skillLinkClick(){
        skillLink.click();
    }

    //endregion

    public StudentWorkspace(WebDriver driver) {

        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}

