package io.mylift.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    //region Elements
    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "password")
    private WebElement passwordField;

    @FindBy(css = "input[value='Login']")
    private  WebElement loginButton;
    //endregion

    //region Methods
    public void loginUser(String email, String password) {

        emailField.sendKeys(email);
        passwordField.sendKeys(password);
        loginButton.submit();
    }
    //endregion

    public HomePage(WebDriver driver) {

        PageFactory.initElements(driver, this);
    }
}
