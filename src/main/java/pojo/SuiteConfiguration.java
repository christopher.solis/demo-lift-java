package pojo;

public class SuiteConfiguration {

    private boolean remoteTesting;
    private String bSOS;
    private String bSOSVersion;
    private String bSProjectName;
    private String bSBuild;

    private String browser;
    private boolean headless;
    private String testEnvironment;
    private String usersGroup;

    private boolean enableTestLink;
    private String projectNameTestLink;
    private String testPlanNameTestLink;
    private String platformTestLink;
    private String suiteNameTestLink;
    private String testCasePathTestLink;

    public boolean isRemoteTesting() {
        return remoteTesting;
    }

    public void setRemoteTesting(boolean remoteTesting) {
        this.remoteTesting = remoteTesting;
    }

    public String getbSOS() {
        return bSOS;
    }

    public void setbSOS(String bSOS) {
        this.bSOS = bSOS;
    }

    public String getbSOSVersion() {
        return bSOSVersion;
    }

    public void setbSOSVersion(String bSOSVersion) {
        this.bSOSVersion = bSOSVersion;
    }

    public String getbSProjectName() {
        return bSProjectName;
    }

    public void setbSProjectName(String bSProjectName) {
        this.bSProjectName = bSProjectName;
    }

    public String getbSBuild() {
        return bSBuild;
    }

    public void setbSBuild(String bSBuild) {
        this.bSBuild = bSBuild;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public boolean isHeadless() {
        return headless;
    }

    public void setHeadless(boolean headless) {
        this.headless = headless;
    }

    public boolean isEnableTestLink() {
        return enableTestLink;
    }

    public void setEnableTestLink(boolean enableTestLink) {
        this.enableTestLink = enableTestLink;
    }

    public String getProjectNameTestLink() {
        return projectNameTestLink;
    }

    public void setProjectNameTestLink(String projectNameTestLink) {
        this.projectNameTestLink = projectNameTestLink;
    }

    public String getTestPlanNameTestLink() {
        return testPlanNameTestLink;
    }

    public void setTestPlanNameTestLink(String testPlanNameTestLink) { this.testPlanNameTestLink = testPlanNameTestLink; }

    public String getPlatformTestLink() {
        return platformTestLink;
    }

    public void setPlatformTestLink(String platformTestLink) {
        this.platformTestLink = platformTestLink;
    }

    public String getSuiteNameTestLink() {
        return suiteNameTestLink;
    }

    public void setSuiteNameTestLink(String suiteNameTestLink) {
        this.suiteNameTestLink = suiteNameTestLink;
    }

    public String getTestCasePathTestLink() {
        return testCasePathTestLink;
    }

    public void setTestCasePathTestLink(String testCasePathTestLink) { this.testCasePathTestLink = testCasePathTestLink; }

    public String getUsersGroup() {
        return usersGroup;
    }

    public void setUsersGroup(String usersGroup) {
        this.usersGroup = usersGroup;
    }

    public String getTestEnvironment() { return testEnvironment; }

    public void setTestEnvironment(String testEnvironment) { this.testEnvironment = testEnvironment; }

    @Override
    public String toString() {
        return "SuiteConfiguration{" +
                "remoteTesting=" + remoteTesting +
                ", bSOS='" + bSOS + '\'' +
                ", bSOSVersion='" + bSOSVersion + '\'' +
                ", bSProjectName='" + bSProjectName + '\'' +
                ", bSBuild='" + bSBuild + '\'' +
                ", browser='" + browser + '\'' +
                ", headless=" + headless +
                ", usersGroup='" + usersGroup + '\'' +
                ", enableTestLink=" + enableTestLink +
                ", projectNameTestLink='" + projectNameTestLink + '\'' +
                ", testPlanNameTestLink='" + testPlanNameTestLink + '\'' +
                ", platformTestLink='" + platformTestLink + '\'' +
                ", suiteNameTestLink='" + suiteNameTestLink + '\'' +
                ", testCasePathTestLink='" + testCasePathTestLink + '\'' +
                '}';
    }
}
