package base;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pojo.SuiteConfiguration;
import utilities.DriverFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;

/**
 * Main Test configuration file
 * Required TestLink and Log4j instances are initialized here
 * TestNG @Before methods are included under this class
 */
public class BaseTest extends PageObjectHandler{

    private static org.slf4j.Logger log = LoggerFactory.getLogger(BaseTest.class);
    protected String date;
    protected boolean formattedDate;
    private SuiteConfiguration suiteConfiguration;

    /**
     * Description:
     * Higher level configurations applied to all methods included under the <suite> tag
     */
    @BeforeSuite(alwaysRun = true)
    public void setUpSuite() {

        date = getCurrentDate(1,0,0);
    }

    /**
     * Description:
     * Higher level configurations applied to all methods included under the <test> tag
     * TestLink instances and date generators are defined here
     * Optional values can be set as default values for each parameter
     * @param configName
     * @throws Exception
     */
    @Parameters({"configName"})
    @BeforeTest(alwaysRun = true)
    public void setUpTest(@Optional("") String configName) throws Exception{

        if(suiteConfiguration == null){ suiteConfiguration = extractConfigurationParameters(configName); }

        date = getCurrentDate(1,0,0);
    }

    /**
     * Description:
     * Starts the browser and applies the specified configurations before each @Test method is executed
     * Optional values can be set as default values for each parameter
     * @param bSTestName
     * @throws MalformedURLException
     */
    @Parameters({"bSTestName"})
    @BeforeMethod(alwaysRun = true)
    public void setUpDriver(@Optional("") String bSTestName, ITestResult result) throws MalformedURLException {

        log.info("Test has started: {} - {}",
                result.getMethod().getMethodName(),
                result.getMethod().getDescription());

        try {

            Hashtable<String, String> bSCapabilities = new Hashtable<String, String>();

            if(suiteConfiguration.isRemoteTesting()){

                bSCapabilities.put("os", suiteConfiguration.getbSOS());
                bSCapabilities.put("osVersion", suiteConfiguration.getbSOSVersion());
                bSCapabilities.put("projectName", suiteConfiguration.getbSProjectName());
                bSCapabilities.put("buildName", suiteConfiguration.getbSBuild());
                bSCapabilities.put("sessionName", bSTestName);
            }

            date = getCurrentDate(1,0,0);
            driver = DriverFactory.open(suiteConfiguration.getBrowser(), suiteConfiguration.isHeadless(), suiteConfiguration.isRemoteTesting(), bSCapabilities);

            if (driver.manage().getCookies() != null){

                driver.manage().deleteAllCookies();
            }

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.get("https://liftstaging.mylift.io/");
            driver.manage().window().maximize();

        } catch (Exception | Error e){

            log.error("Error while setting up the test", e);
            throw e;
        }
    }

    /**
     * Description:
     * Actions executed after each @Test is executed
     */
    @AfterMethod(alwaysRun = true)
    public void teardown(ITestResult result){

        if(!result.isSuccess()) {

            log.error("Test Failed", result.getThrowable());

            try {

                uploadLogs();
            } catch (Exception | Error e){

                log.warn("There was a problem while trying to upload the log file: {}", e.getMessage());
            }
        }

        try{

            takeScreenshot();
            pagesTeardown();
            driver.quit();

        }catch(Exception | Error e){

            log.warn("There was a problem at the teardown step: {}", e.getMessage());
        }
    }

    /**
     * * Description:
     * Takes a screenshot for the Allure Reporting tool
     * @return Byte array with the screenshot
     */
    @Attachment(value = "screenshot", type = "image/png")
    public byte[] takeScreenshot(){

        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    /**
     * * Description:
     * Takes a screenshot for the Allure Reporting tool
     * @return Byte array with the screenshot
     */
    @Attachment(value = "executionlogs")
    public String uploadLogs() throws IOException{

        try{

            return FileUtils.readFileToString(new File("logs/Demo.log"));
        } catch (Exception e){

            log.error("Error while reading the log file");
            throw e;
        }
    }

    /**
     * Description:
     * Extracts the test configuration from a JSON file
     * @param configName
     * @return SuiteConfiguration object with the configuration data extracted from the JSON file
     * @throws FileNotFoundException
     */
    public SuiteConfiguration extractConfigurationParameters(String configName) throws FileNotFoundException {

        try{

            JsonElement dataSet = new JsonParser()
                    .parse(new FileReader("src/test/resources/configuration/config.json"))
                    .getAsJsonObject()
                    .get(configName);

            return new Gson().fromJson(dataSet, SuiteConfiguration.class);
        } catch (Exception e){

            log.error("Error while reading the config file");
            throw e;
        }
    }

    /**
     * Description:
     * Generates the current date on a string format
     * @param sumMonth
     * @param sumDay
     * @param sumYear
     * @return String with the current date
     */
    protected String getCurrentDate(int sumMonth, int sumDay, int sumYear) {

        try {

            String currentDate;
            Date date = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            String year = Integer.toString(cal.get(Calendar.YEAR) + sumYear);
            String month = Integer.toString(cal.get(Calendar.MONTH) + sumMonth);
            String day = Integer.toString(cal.get(Calendar.DAY_OF_MONTH) + sumDay);
            String hour = Integer.toString(cal.get(Calendar.HOUR));
            String min = Integer.toString(cal.get(Calendar.MINUTE));
            String sec = Integer.toString(cal.get(Calendar.SECOND));

            if(formattedDate){

                currentDate = String.format("%s/%s/%s", month, day, year);
                formattedDate = false;

            }else{

                currentDate = month + day + year + hour + min + sec;
            }

            return currentDate;

        } catch (Exception e) {

            log.error("Error while generating the current date");
            throw e;
        }
    }

    public void addWait(int millis) {

        try {

            Thread.sleep(millis);
        } catch (Exception e){

            log.warn(" Error on the 'addWait' method.");
        }
    }

    public void waitForLoadingDiv(){

        try{

            waitForElementVisible(driver.findElement(By.id("loading_div")), 5);

        }catch(Exception e){

            log.warn("Loading div element not found");
        }

        waitForElementInvisible(driver.findElement(By.id("loading_div")), 15);
    }

    public void waitForElementClickable(WebElement element){

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitForElementVisible(WebElement element){

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitForElementVisible(WebElement element, long timeOutInSeconds){

        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitForElementInvisible(WebElement element){

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.invisibilityOf(element));
    }

    public void waitForElementInvisible(WebElement element, long timeOutInSeconds){

        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(ExpectedConditions.invisibilityOf(element));
    }

    public void waitForTextToBeInElement(WebElement element, String text){

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.textToBePresentInElement(element, text));
    }

    public void waitForPresenceOfElementLocatedByXpath(String xpath){

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
    }

    public void waitForUrlToContain(String url)
    {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.urlContains(url));
    }

    public void navigateToRightTab()
    {
        Actions action = new Actions(driver);
        action.keyDown(Keys.COMMAND).sendKeys(Keys.TAB).build().perform();
    }

    public void pressEnterKey()
    {
        Actions action = new Actions(driver);
        action.keyDown(Keys.COMMAND).sendKeys(Keys.ENTER).build().perform();
    }

    public void moveToNewTab()
    {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.numberOfWindowsToBe(2));

        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
    }

    public void navigateToLeftTab()
    {
        Actions action = new Actions(driver);
        action.keyDown(Keys.COMMAND).keyDown(Keys.SHIFT).sendKeys(Keys.TAB).build().perform();
    }

    public void scrollTillEnd()
    {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    public void scrollTillElement(WebElement element)
    {

        log.info("Scrolling the page to a element");

        try{

            JavascriptExecutor jse = (JavascriptExecutor)driver;
            jse.executeScript("arguments[0].scrollIntoView();", element);
        } catch (Exception | Error e){

            log.error("Error while scrolling the page to the element");
            throw e;
        }
    }

    public void scrollDown()
    {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,800)","");
    }

    public void scrollUp()
    {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("scroll(0,-800)","");
    }

    public void halfScroll()
    {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("scroll(0,-800)","");
        jse.executeScript("scroll(0,400)","");
    }

    public void clickUsingJavascriptExecutor(WebElement element)
    {

        log.info("Clicking an element using Javascript");

        try{

            JavascriptExecutor jse = (JavascriptExecutor)driver;
            jse.executeScript("arguments[0].click();", element);
        }catch (Exception | Error e){

            log.error("Error while clicking an element with Javascript");
            throw e;
        }

    }

    public void fillUsingJavascriptExecutor(WebElement element, String input)
    {

        log.info("Filling an element using Javascript with '{}'", input);

        try{

            JavascriptExecutor jse = (JavascriptExecutor)driver;
            jse.executeScript("arguments[0].setAttribute('value', '" + input + "');", element);
        }catch (Exception | Error e){

            log.error("Error while filling an element with Javascript");
            throw e;
        }
    }
}