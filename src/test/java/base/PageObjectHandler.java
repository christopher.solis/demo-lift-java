package base;

import io.mylift.pageobjects.*;
import org.openqa.selenium.WebDriver;

/**
 * Description:
 * Lazy loading class where every Page Object class is going to be instantiated
 */
public class PageObjectHandler {

    public WebDriver driver;

    //region Pages
    private HomePage homePage;
    private TeacherDashboardPage teacherDashboardPage;
    private ClassWorkspacePage classWorkspacePage;
    private StudentWorkspace studentWorkspace;
    private StudentPortfolio studentPortfolio;
    //endregion

    //region Methods
    public void pagesTeardown(){

        homePage = null;
        teacherDashboardPage = null;
        classWorkspacePage = null;
        studentWorkspace = null;

    }
    //endregion

    //region Getters
    public HomePage homePage() {
        if( homePage == null){
            homePage = new HomePage(driver);
        }
        return homePage;
    }

    public TeacherDashboardPage teacherDashboardPage() {
        if( teacherDashboardPage == null){
            teacherDashboardPage = new TeacherDashboardPage(driver);
        }
        return teacherDashboardPage;
    }

    public ClassWorkspacePage classWorkspacePage() {
        if( classWorkspacePage == null){
            classWorkspacePage = new ClassWorkspacePage(driver);
        }
        return classWorkspacePage;
    }

    public StudentWorkspace studentWorkspace(){
        if(studentWorkspace == null){
            studentWorkspace = new StudentWorkspace(driver);
        }
        return studentWorkspace;
    }

    public StudentPortfolio studentPortfolio(){
        if(studentPortfolio == null){
            studentPortfolio = new StudentPortfolio(driver);
        }
        return studentPortfolio;
    }
    //endregion
}
