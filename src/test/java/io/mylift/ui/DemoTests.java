package io.mylift.ui;

import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DemoTests extends BaseTest {

    @Test
    public void loginTeacherTest() {

        homePage().loginUser("gerardo.aragon+tp01@edify.cr", "Asdqwe123$");

        Assert.assertTrue(teacherDashboardPage().isTeacherDashboard(), "The current page is not the 'Teacher Dashboard' page.");
    }

    @Test
    public void createNewProject() {

        String projectName = "Demo " + date;

        homePage().loginUser("gerardo.aragon+tp01@edify.cr", "Asdqwe123$");
        teacherDashboardPage().selectClassByName("Class 01");
        classWorkspacePage().createNewProject(projectName, "Demo Description");

        waitForElementVisible(classWorkspacePage().getAlertMessage());
        Assert.assertTrue(classWorkspacePage().projectCreatedSuccessfully(), String.format("The project '%s' was not created successfully", projectName));
    }

    @Test
    public void StudentRateSkill () {

        homePage().loginUser("gerardo.aragon+stp01@edify.cr", "Asdqwe123$");

        studentWorkspace().expandAllButtonClick();
        studentWorkspace().skillLinkClick();
        studentPortfolio().gettingStartedLevelClick();
        studentPortfolio().rateButtonClick();
        studentPortfolio().yesButtonClick();

        addWait(2000);
        Assert.assertTrue(studentPortfolio().getNotification().contains("Rating successfully performed"));
    }
}
